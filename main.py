# This Python file contains various vulnerabilities for educational purposes

import os

# Vulnerability 1: Command Injection
def execute_command(command):
    os.system(command)

# Vulnerability 2: SQL Injection
def fetch_user_data(username):
    query = "SELECT * FROM users WHERE username = '" + username + "'"
    # Execute the SQL query and fetch user data

# Vulnerability 3: Cross-Site Scripting (XSS)
def display_message(message):
    print("<div>" + message + "</div>")

# Vulnerability 4: Insecure Deserialization
import pickle

class User:
    def __init__(self, username):
        self.username = username

    def __reduce__(self):
        return (os.system, ("echo 'Vulnerable'",))

def load_user_data(data):
    user = pickle.loads(data)
    # Use user data

# Vulnerability 5: Insecure File Handling
def read_file(filename):
    with open(filename, 'r') as file:
        content = file.read()
    return content

# Vulnerability 6: Weak Password Policy
def check_password(password):
    if len(password) < 8:
        return "Weak password"
    else:
        return "Strong password"

# Vulnerability 7: Unvalidated Redirects and Forwards
from flask import Flask, redirect, request

app = Flask(__name__)

@app.route('/redirect')
def redirect_to_url():
    url = request.args.get('url')
    return redirect(url)

# Vulnerability 8: Directory Traversal
def load_file(file_path):
    with open(file_path, 'r') as file:
        content = file.read()
    return content

if __name__ == '__main__':
    app.run(debug=True)
