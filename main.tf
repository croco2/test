provider "aws" {
  region = "eu-north-1"
}

# resource "aws_security_group" "web" {
#   name        = "nginx_security_group"
#   description = "Configure required ports"

#   ingress {
#     from_port   = 22
#     to_port     = 22
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   ingress {
#     from_port   = 80
#     to_port     = 80
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   ingress {
#     from_port   = 8088
#     to_port     = 8088
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }

resource "aws_instance" "ec2_instance" {
  ami           = "ami-0fe8bec493a81c7da"
  instance_type = "t3.micro"

  key_name        = "croco"
  security_groups = ["launch-wizard-2"] # or the web security group

  tags = {
    Name = "ubuntu instance"
  }

  user_data = file("main.sh")

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("../croco.pem")
    host        = aws_instance.ec2_instance.public_ip
  }
}



