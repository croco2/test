---
title: CI/CD
---
```mermaid
graph TD

subgraph Build
  buildDocker[build docker]
  deployDocker[deploy]
end

subgraph "buildDocker"["stage: build"]
  A[before_script]
  B[script]
  C[hello]
  D[docker build]
  E[docker push]

  A --> C
  B --> D
  D --> E
end

subgraph "deployDocker"["stage: deploy"]
  F[before_script]
  G[script]
  H[scp Dockerfile]
  I[scp docker-compose.yaml]
  J[scp main.py]
  K[ssh mkdir]
  L[scp response.json]
  M[ssh docker compose up]

  F --> H
  F --> I
  F --> J
  F --> K
  K --> L
  K --> M
end

buildDocker --> deployDocker

```

```markdown

```

<SwmSnippet path="/.gitlab-ci.yml" line="1">

---

This code snippet defines a list of stages, including "build" and "deploy".

&nbsp;

```mermaid
stateDiagram-v2
    [*] --> build
    build --> deploy

```

```yaml
stages:
  - build
  - deploy
```

---

</SwmSnippet>

<SwmSnippet path="/.gitlab-ci.yml" line="5">

---

This code snippet builds a Docker image and pushes it to a Docker registry. It uses the Docker-in-Docker service to enable Docker functionality within the CI/CD pipeline. The `before_script` section logs into the Docker registry using the provided credentials. The `script` section builds the Docker image with a specific tag and then pushes it to the registry.

```yaml
build docker:
  stage: build
  image: docker
  services:
    - docker:dind
  before_script:
    - docker login -u $REGISTRY_USER -p $REGISTRY_PASSWORD
    - test
  script:
    - docker build -t palaga123/croco:python-api-$CI_COMMIT_SHORT_SHA .
    - docker push palaga123/croco:python-api-$CI_COMMIT_SHORT_SHA
```

---

</SwmSnippet>

<SwmSnippet path="/.gitlab-ci.yml" line="17">

---

This code snippet is used to deploy a project to a remote server. It sets up the necessary dependencies and executes a series of commands to copy files, create directories, and run a Docker container on the remote server.

```yaml
deploy:
  stage: deploy
  image: alpine
  before_script:
    - apk add openssh-client
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' > private_key.pem
    - chmod 600 private_key.pem
    - ssh-add private_key.pem
  script:
    - scp -o StrictHostKeyChecking=no Dockerfile $AWS_DNS:~/
    - scp -o StrictHostKeyChecking=no docker-compose.yaml $AWS_DNS:~/
    - scp -o StrictHostKeyChecking=no main.py $AWS_DNS:~/
    - ssh -o StrictHostKeyChecking=no $AWS_DNS "mkdir -p test"
    - scp -o StrictHostKeyChecking=no test/response.json $AWS_DNS:~/test/
    - ssh -o StrictHostKeyChecking=no $AWS_DNS "sudo docker compose up -d"
```

---

</SwmSnippet>

<SwmMeta version="3.0.0" repo-id="Z2l0bGFiJTNBJTNBdGVzdCUzQSUzQWNyb2NvMg==" repo-name="test"><sup>Powered by [Swimm](https://app.swimm.io/)</sup></SwmMeta>
