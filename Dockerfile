FROM python:3.8-slim

WORKDIR /app

COPY . /app

RUN pip install Flask

EXPOSE 8088

ENV BASE_FOLDER /app
ENV RESOURCE_DIR /app/test

CMD ["python", "main.py"]
